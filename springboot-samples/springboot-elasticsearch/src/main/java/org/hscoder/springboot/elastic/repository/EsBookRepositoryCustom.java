package org.hscoder.springboot.elastic.repository;

import org.hscoder.springboot.elastic.domain.EsBook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EsBookRepositoryCustom {

    public Page<EsBook> search(String type, String author, String title, List<String> tags, Pageable pageable);
}
