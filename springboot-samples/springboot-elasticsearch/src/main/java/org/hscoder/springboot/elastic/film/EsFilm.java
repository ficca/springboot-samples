package org.hscoder.springboot.elastic.film;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;

@Document(type="dytt", indexName = "film")
public class EsFilm {

    @Id
    private String id;
    //来源URL
    private String url;

    //片名
    private String name;

    //原名
    private String localName;

    //地区
    @Field(type= FieldType.Keyword)
    private String region;
    //年代
    @Field(type= FieldType.Keyword)
    private String period;
    //语言
    @Field(type= FieldType.Keyword)
    private String lanuage;
    //片长
    @Field(index = false)
    private String during;

    //评分(豆瓣)
    private double score;

    //上映日期
    private Date releaseDate;

    //分类
    @Field(type= FieldType.Keyword)
    private List<String> sorts;
    //标签
    @Field(type= FieldType.Keyword)
    private List<String> tags;
    //导演
    private String director;
    //编剧
    private String scriptwriter;
    //演员
    private List<String> actors;
    //简介
    //语言
    @Field(type= FieldType.Text)
    private String intro;

    //封面照
    @Field(index = false)
    private String coverImgUrl;
    //剧照
    @Field(index = false)
    private List<String> captureImgUrls;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getLanuage() {
        return lanuage;
    }

    public void setLanuage(String lanuage) {
        this.lanuage = lanuage;
    }

    public String getDuring() {
        return during;
    }

    public void setDuring(String during) {
        this.during = during;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<String> getSorts() {
        return sorts;
    }

    public void setSorts(List<String> sorts) {
        this.sorts = sorts;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getScriptwriter() {
        return scriptwriter;
    }

    public void setScriptwriter(String scriptwriter) {
        this.scriptwriter = scriptwriter;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCoverImgUrl() {
        return coverImgUrl;
    }

    public void setCoverImgUrl(String coverImgUrl) {
        this.coverImgUrl = coverImgUrl;
    }

    public List<String> getCaptureImgUrls() {
        return captureImgUrls;
    }

    public void setCaptureImgUrls(List<String> captureImgUrls) {
        this.captureImgUrls = captureImgUrls;
    }
}
