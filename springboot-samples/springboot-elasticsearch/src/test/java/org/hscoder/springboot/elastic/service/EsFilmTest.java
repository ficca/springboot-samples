package org.hscoder.springboot.elastic.service;

import org.hscoder.springboot.elastic.ElasticBoot;
import org.hscoder.springboot.elastic.film.EsFilm;
import org.hscoder.springboot.elastic.film.EsFilmImporter;
import org.hscoder.springboot.elastic.film.EsFilmRepository;
import org.hscoder.springboot.elastic.film.EsFilmService;
import org.hscoder.springboot.elastic.util.JsonUtil;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElasticBoot.class)
public class EsFilmTest {

    @Autowired
    private EsFilmService filmService;

    @Autowired
    private EsFilmRepository filmRepository;

    @Autowired
    private EsFilmImporter importer;

    //@Before
    public void initData() {
        filmRepository.deleteAll();
        importer.importFromLocal("D:/temp/dytt1");
    }

    @After
    public void disposeData() {

    }

    @Test
    public void testListByPeriod() {
        List<EsFilm> films = filmService.listByPeriod("2019");
        System.out.println(JsonUtil.toPrettyJson(films));
    }
}
