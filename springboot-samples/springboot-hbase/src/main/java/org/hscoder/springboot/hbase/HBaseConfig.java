package org.hscoder.springboot.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.hadoop.hbase.HbaseTemplate;

@org.springframework.context.annotation.Configuration
public class HBaseConfig {

    @Value("${hbase.zookeeper.property.clientPort}")
    private String zookeeperClientPort;

    @Value("${hbase.zookeeper.quorum}")
    private String zookeeperQuorum;

    @Bean
    public HbaseTemplate hbaseTemplate(Configuration configuration) {
        return new HbaseTemplate(configuration);
    }

    @Bean
    public Configuration hbaseConfiguration() {
        org.apache.hadoop.conf.Configuration config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.property.clientPort", zookeeperClientPort);
        config.set("hbase.zookeeper.quorum", zookeeperQuorum);
        return config;
    }


}
