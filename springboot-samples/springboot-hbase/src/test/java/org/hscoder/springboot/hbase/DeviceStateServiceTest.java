package org.hscoder.springboot.hbase;

import org.hscoder.springboot.simplebuild.util.JsonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HBaseBoot.class)
public class DeviceStateServiceTest {

    @Autowired
    private DeviceStateService deviceStateService;

    @Test
    public void testCrud(){

        deviceStateService.add("rowkey1", "电视机", "关闭");
        deviceStateService.add("rowkey2", "空调", "打开");

        DeviceState row1 = deviceStateService.get("rowkey1");
        assertNotNull(row1);
        assertEquals(row1.getName(), "电视机");

        List<DeviceState> rows = deviceStateService.find("row", "rowkey1", null, 10);
        assertNotNull(rows);
        assertTrue(((List) rows).size() == 2);
        System.out.println("find 10 rows: -- \n" + JsonUtil.toPrettyJson(rows));

        rows = deviceStateService.find("row", "rowkey1", null, 1);
        assertNotNull(rows);
        System.out.println("find 1 rows: -- \n" + JsonUtil.toPrettyJson(rows));

        rows = deviceStateService.find("row", "rowkey3", null, 10);
        assertNotNull(rows);
        System.out.println("find 10 rows, from rowkey3: -- \n" + JsonUtil.toPrettyJson(rows));

        deviceStateService.delete("rowkey1");
        rows = deviceStateService.find("row", "rowkey1", null, 10);
        assertNotNull(rows);
        System.out.println("after delete row1, find 10: -- \n" + JsonUtil.toPrettyJson(rows));

        //delete other one
        deviceStateService.delete("rowkey2");
    }
}
