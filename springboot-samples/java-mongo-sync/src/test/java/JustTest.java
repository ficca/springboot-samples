import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JustTest {

    public static void main(String[] args) throws InterruptedException, IOException {

        String url = "https://blog.51cto.com/14254788/2430458";

      //  url = "https://blog.51cto.com/14254788/2419516";
        for(int i=0; i<3; i++){
            String content = IOUtils.toString(new URL(url), "utf-8");
            System.out.println("阅读数：" + readCount(content));
            Thread.sleep(1000);
        }
    }

    public static int readCount(String content){
        String pattern = "(\\d+)人阅读";
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(content);
        if(((Matcher) matcher).find()){
            return Integer.parseInt(matcher.group(1));
        }
        return -1;
    }
}
