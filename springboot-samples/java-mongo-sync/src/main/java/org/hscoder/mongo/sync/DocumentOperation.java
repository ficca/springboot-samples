package org.hscoder.mongo.sync;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.function.Consumer;


public class DocumentOperation {

    public static void find(MongoClient client){

        MongoDatabase database = client.getDatabase("appdb");
        database.listCollectionNames().forEach((Consumer<String>) c -> {
            System.out.println(c);
        });

        MongoCollection<Document> collection = database.getCollection("test");

    }
}
